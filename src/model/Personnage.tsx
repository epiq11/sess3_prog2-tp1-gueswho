export interface Personnage {
    name: string;
    path: string;
    done?: boolean;
}
