import { CartePersonnage } from 'component/CartePersonnage';
import { PersonneATrouver } from 'component/personneatrouver';
import { Personnage } from 'model/Personnage';
import React from 'react';

interface Props { }

interface State { list: Personnage[]; selection: Personnage; }

export class JeuxGuestWho extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            list: [
                { name: 'Black Canary', path: 'img/Black_Canary.png' },
                { name: 'Black Siren', path: 'img/black-siren.png' },
                { name: 'Curtis Holt', path: 'img/Curtis_Holt.png' },
                { name: 'Dinah Drake', path: 'img/Dinah_Drake.png' },
                { name: 'Felicity Smoak', path: 'img/Felicity_Smoak.png' },
                { name: 'John Diggle', path: 'img/John_Diggle.png' },
                { name: 'Lorel Lance', path: 'img/Lorel_Lance.png' },
                { name: 'Green Arrow', path: 'img/Green_Arrow.png' },
                { name: 'Malcolm Merlyn', path: 'img/malcolm_merlyn.png' },
                { name: 'Mister Terrific', path: 'img/Mister_Terrific.png' },
                { name: 'Nyssa al Ghul', path: 'img/Nyssa_al_Ghul.png' },
                { name: 'Oliver Queen', path: 'img/Oliver_Queen.png' },
                { name: 'Quentin Lance', path: 'img/Quentin_Lance.png' },
                { name: 'Ras al Ghul', path: 'img/Ras_al_Ghul.png' },
                { name: 'Rene Ramirez', path: 'img/Rene_Ramirez.png' },
                { name: 'Robert Queen', path: 'img/Robert_Queen.png' },
                { name: 'Roy Harper', path: 'img/Roy_Harper.png' },
                { name: 'Sara Lance', path: 'img/Sara_Lance.png' },
                { name: 'Shado', path: 'img/Shado.png' },
                { name: 'Slade Wilson', path: 'img/Slade_Wilson.png' },
                { name: 'Speedy', path: 'img/Speedy.png' },
                { name: 'Tea Queen', path: 'img/Tea_dearden_Queen.png' },
                { name: 'The Flash', path: 'img/The_Flash.png' },
                { name: 'Tommy Merlyn', path: 'img/Tommy_Merlyn.png' },
            ],
            selection: { name: '', path: 'img/selectionner_une_image.png' }
        };
    }

    public render() {

        return <>
            <section className='container sectionDuHaut'>
                <h1>Jeux: Guess Who</h1>
                <div className='containerItem'>
                    <PersonneATrouver selection={this.state.selection} />
                </div>
                <button onClick={() => this.resetByDefault()}>Recommencer</button>

            </section>

            <section className='container'>
                {this.state.list.map(item =>

                    <CartePersonnage key={item.name} item={item} selection={this.state.selection}
                        swapDone={() => this.swapDone(item)}
                        selectPersonne={() => this.selectPersonne(item)}
                    />)
                }
            </section>
        </>;
    }

    private selectPersonne = (item: Personnage) => {
        this.state.selection.name = item.name;
        this.state.selection.path = item.path;
        this.setState({ selection: this.state.selection });
    };

    private swapDone = (item: Personnage) => {
        item.done = !item.done;
        this.setState({ list: this.state.list });
    };

    private resetByDefault = () => {
        this.state.list.map(item =>
            item.done = false);

        this.state.selection.name = '';
        this.state.selection.path = 'img/selectionner_une_image.png';

        this.setState({ list: this.state.list, selection: this.state.selection });
    };


}
