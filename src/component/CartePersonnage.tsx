import { Personnage } from 'model/Personnage';
import React from 'react';

interface Props { item: Personnage; selection: Personnage; swapDone(): void; selectPersonne(): void; }

interface State { }

export class CartePersonnage extends React.Component<Props, State> {

    public render() {
        return <div className='containerItem flexBasis15' onClick={this.props.selection.name === '' ? this.props.selectPersonne : this.props.swapDone}>
            <img src={this.props.item.done ? 'img/x_red.png' : this.props.item.path} alt={this.props.item.name} />
            <h2>{this.props.item.name}</h2>
        </div>;
    }

}
