import { Personnage } from 'model/Personnage';
import React from 'react';

interface Props { selection: Personnage; }

interface State { }

export class PersonneATrouver extends React.Component<Props, State> {

    public render() {
        return <>
            <img src={this.props.selection.path} alt={this.props.selection.name} />
            <h2>{this.props.selection.name}</h2>
        </>;
    }

}
