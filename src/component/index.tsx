import React from 'react';
import { Link } from 'react-router-dom';

interface Props { }

export class Index extends React.Component<Props> {
    public render() {
        return <section className='container'>

            <h1>Jeux Guess Who </h1>
            <p>
                Vous devez choisir un personnage en le sélectionnant avec la souris. Par la suite, vous vous posez des questions en alternant pour deviner le personnage de l’autre. Vous éliminez ceux qui ne sont définitivement pas le personnage de l’autre joueur, jusqu’à ce qu’il n’en reste plus qu’un. La première personne à deviner le personnage de l’autre gagne.
            </p>
            <button><Link className='lien' to='/guesswho'>Jouer</Link></button>
        </section>;
    }
}
