import { Index } from 'component';
import { JeuxGuestWho } from 'JeuxGuestWho';
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

export class Router extends React.Component<{}> {

    public render() {
        return <BrowserRouter>
            <Switch>
                <Route path='/guesswho'>
                    <main>
                        <JeuxGuestWho />
                    </main>
                </Route>
                <Route path='/'><Index /></Route>
            </Switch>
        </BrowserRouter>;
    }
}
